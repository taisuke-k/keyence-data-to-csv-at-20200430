
## はじめに
ビジョン履歴のCSVデータは "Distortion" データベースに格納されています。
データベースは、"LH" と "RH" でコレクションは分けられています。

以降の例では、LHのデータを抽出する方法を示しています。
対象をLHからRHに切り替える場合は、--collection=LH を --collection=RH に置き換えて下さい。
また、--out=Distortion_LH.csvを--out=Distortion_RH.csvに置き換えて下さい。
</br>
以降の例では、CSVファイルにはタイムスタンプを昇順で出力します。
降順で出力する場合は、--sort='{timestamp: 1}' を --sort='{timestamp: -1}' に置き換えて下さい。

### 全データをCSVファイルに出力する
```
mongoexport --db=Distortion --collection=LH --type=csv \
  --fields=UpAndDownDate,UpAndDownTime,UpAndDownSerialNo,UpAndDownCarModelNo,UpAndDownSukibaNo,UpAndDown1Unit,UpAndDown2Unit1,UpAndDown2Unit2,UpAndDown3Unit,UpAndDown4Unit,UpAndDown5Unit1,UpAndDown5Unit2,UpAndDown5Unit3,UpAndDown6Unit,UpAndDown7Unit,UpAndDown8Unit,UpAndDown9Unit,UpAndDown10Unit,LeftAndRightDate,LeftAndRightTime,LeftAndRightSerialNo,LeftAndRightCarModelNo,LeftAndRightSukibaNo,LeftAndRight1Unit,LeftAndRight2Unit1,LeftAndRight2Unit2,LeftAndRight3Unit,LeftAndRight4Unit,LeftAndRight5Unit1,LeftAndRight5Unit2,LeftAndRight5Unit3,LeftAndRight6Unit,LeftAndRight7Unit,LeftAndRight8Unit,LeftAndRight9Unit,LeftAndRight10Unit,timestamp \
  --sort='{timestamp: 1}' \
  --out=Distortion_LH.csv
```

### 期間 2019/01/01 ~ 2020/01/01 のみをデータをCSVに出力する
```
mongoexport --db=Distortion --collection=LH --type=csv \
  --fields=UpAndDownDate,UpAndDownTime,UpAndDownSerialNo,UpAndDownCarModelNo,UpAndDownSukibaNo,UpAndDown1Unit,UpAndDown2Unit1,UpAndDown2Unit2,UpAndDown3Unit,UpAndDown4Unit,UpAndDown5Unit1,UpAndDown5Unit2,UpAndDown5Unit3,UpAndDown6Unit,UpAndDown7Unit,UpAndDown8Unit,UpAndDown9Unit,UpAndDown10Unit,LeftAndRightDate,LeftAndRightTime,LeftAndRightSerialNo,LeftAndRightCarModelNo,LeftAndRightSukibaNo,LeftAndRight1Unit,LeftAndRight2Unit1,LeftAndRight2Unit2,LeftAndRight3Unit,LeftAndRight4Unit,LeftAndRight5Unit1,LeftAndRight5Unit2,LeftAndRight5Unit3,LeftAndRight6Unit,LeftAndRight7Unit,LeftAndRight8Unit,LeftAndRight9Unit,LeftAndRight10Unit,timestamp \
  --query='{"timestamp": {"$gte": {"$date": "2019-01-01T00:00:00.000Z"}, "$lt": {"$date": "2020-01-01T00:00:00.000Z"}}}' \
  --sort='{timestamp: 1}' \
  --out=Distortion_LH.csv
```

### 上下車種="44" のみをCSVに出力する
```
mongoexport --db=Distortion --collection=LH --type=csv \
  --fields=UpAndDownDate,UpAndDownTime,UpAndDownSerialNo,UpAndDownCarModelNo,UpAndDownSukibaNo,UpAndDown1Unit,UpAndDown2Unit1,UpAndDown2Unit2,UpAndDown3Unit,UpAndDown4Unit,UpAndDown5Unit1,UpAndDown5Unit2,UpAndDown5Unit3,UpAndDown6Unit,UpAndDown7Unit,UpAndDown8Unit,UpAndDown9Unit,UpAndDown10Unit,LeftAndRightDate,LeftAndRightTime,LeftAndRightSerialNo,LeftAndRightCarModelNo,LeftAndRightSukibaNo,LeftAndRight1Unit,LeftAndRight2Unit1,LeftAndRight2Unit2,LeftAndRight3Unit,LeftAndRight4Unit,LeftAndRight5Unit1,LeftAndRight5Unit2,LeftAndRight5Unit3,LeftAndRight6Unit,LeftAndRight7Unit,LeftAndRight8Unit,LeftAndRight9Unit,LeftAndRight10Unit,timestamp \
  --query='{"UpAndDownCarModelNo": {"$eq": "44"}}' \
  --sort='{timestamp: 1}' \
  --out=Distortion_LH.csv
```

### 左右車種="44" のみをCSVに出力する
```
mongoexport --db=Distortion --collection=LH --type=csv \
  --fields=UpAndDownDate,UpAndDownTime,UpAndDownSerialNo,UpAndDownCarModelNo,UpAndDownSukibaNo,UpAndDown1Unit,UpAndDown2Unit1,UpAndDown2Unit2,UpAndDown3Unit,UpAndDown4Unit,UpAndDown5Unit1,UpAndDown5Unit2,UpAndDown5Unit3,UpAndDown6Unit,UpAndDown7Unit,UpAndDown8Unit,UpAndDown9Unit,UpAndDown10Unit,LeftAndRightDate,LeftAndRightTime,LeftAndRightSerialNo,LeftAndRightCarModelNo,LeftAndRightSukibaNo,LeftAndRight1Unit,LeftAndRight2Unit1,LeftAndRight2Unit2,LeftAndRight3Unit,LeftAndRight4Unit,LeftAndRight5Unit1,LeftAndRight5Unit2,LeftAndRight5Unit3,LeftAndRight6Unit,LeftAndRight7Unit,LeftAndRight8Unit,LeftAndRight9Unit,LeftAndRight10Unit,timestamp \
  --query='{"LeftAndRightCarModelNo": {"$eq": "44"}}' \
  --sort='{timestamp: 1}' \
  --out=Distortion_LH.csv
```

### 上下スキーバ="1" のみをCSVに出力する
```
mongoexport --db=Distortion --collection=LH --type=csv \
  --fields=UpAndDownDate,UpAndDownTime,UpAndDownSerialNo,UpAndDownCarModelNo,UpAndDownSukibaNo,UpAndDown1Unit,UpAndDown2Unit1,UpAndDown2Unit2,UpAndDown3Unit,UpAndDown4Unit,UpAndDown5Unit1,UpAndDown5Unit2,UpAndDown5Unit3,UpAndDown6Unit,UpAndDown7Unit,UpAndDown8Unit,UpAndDown9Unit,UpAndDown10Unit,LeftAndRightDate,LeftAndRightTime,LeftAndRightSerialNo,LeftAndRightCarModelNo,LeftAndRightSukibaNo,LeftAndRight1Unit,LeftAndRight2Unit1,LeftAndRight2Unit2,LeftAndRight3Unit,LeftAndRight4Unit,LeftAndRight5Unit1,LeftAndRight5Unit2,LeftAndRight5Unit3,LeftAndRight6Unit,LeftAndRight7Unit,LeftAndRight8Unit,LeftAndRight9Unit,LeftAndRight10Unit,timestamp \
  --query='{"UpAndDownSukibaNo": {"$eq": "1"}}' \
  --sort='{timestamp: 1}' \
  --out=Distortion_LH.csv
```

### 左右スキーバ="1" のみをCSVに出力する
```
mongoexport --db=Distortion --collection=LH --type=csv \
  --fields=UpAndDownDate,UpAndDownTime,UpAndDownSerialNo,UpAndDownCarModelNo,UpAndDownSukibaNo,UpAndDown1Unit,UpAndDown2Unit1,UpAndDown2Unit2,UpAndDown3Unit,UpAndDown4Unit,UpAndDown5Unit1,UpAndDown5Unit2,UpAndDown5Unit3,UpAndDown6Unit,UpAndDown7Unit,UpAndDown8Unit,UpAndDown9Unit,UpAndDown10Unit,LeftAndRightDate,LeftAndRightTime,LeftAndRightSerialNo,LeftAndRightCarModelNo,LeftAndRightSukibaNo,LeftAndRight1Unit,LeftAndRight2Unit1,LeftAndRight2Unit2,LeftAndRight3Unit,LeftAndRight4Unit,LeftAndRight5Unit1,LeftAndRight5Unit2,LeftAndRight5Unit3,LeftAndRight6Unit,LeftAndRight7Unit,LeftAndRight8Unit,LeftAndRight9Unit,LeftAndRight10Unit,timestamp \
  --query='{"LeftAndRightSukibaNo": {"$eq": "1"}}' \
  --sort='{timestamp: 1}' \
  --out=Distortion_LH.csv
```

### 期間 2019/01/01 ~ 2020/01/01, 車種="44", スキーバ="1" を条件として、CSVヘッダーを車種、スキーバ、指定部位(SC01H,SC01L,SC20W),タイムスタンプを指定してCSVに出力する
```
mongoexport --db=Distortion --collection=LH --type=csv \
  --fields=UpAndDownDate,UpAndDownTime,UpAndDownSerialNo,UpAndDownCarModelNo,UpAndDownSukibaNo,UpAndDown1Unit,UpAndDown2Unit1,UpAndDown2Unit2,UpAndDown3Unit,UpAndDown4Unit,UpAndDown5Unit1,UpAndDown5Unit2,UpAndDown5Unit3,UpAndDown6Unit,UpAndDown7Unit,UpAndDown8Unit,UpAndDown9Unit,UpAndDown10Unit,LeftAndRightDate,LeftAndRightTime,LeftAndRightSerialNo,LeftAndRightCarModelNo,LeftAndRightSukibaNo,LeftAndRight1Unit,LeftAndRight2Unit1,LeftAndRight2Unit2,LeftAndRight3Unit,LeftAndRight4Unit,LeftAndRight5Unit1,LeftAndRight5Unit2,LeftAndRight5Unit3,LeftAndRight6Unit,LeftAndRight7Unit,LeftAndRight8Unit,LeftAndRight9Unit,LeftAndRight10Unit,timestamp \
  --query='{"UpAndDownCarModelNo": {"$eq": "44"}, "UpAndDownSukibaNo": {"$eq": "1"}, "timestamp": {"$gte": {"$date": "2019-01-01T00:00:00.000Z"}, "$lt": {"$date": "2020-01-01T00:00:00.000Z"}}}' \
  --sort='{timestamp: 1}' \
  --out=Distortion_LH.csv
```
