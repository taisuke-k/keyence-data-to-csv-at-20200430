
## はじめに
ビジョン履歴のCSVデータは "Vision" データベースに格納されています。
データベースは、"LH" と "RH" でコレクションは分けられています。

以降の例では、LHのデータを抽出する方法を示しています。
対象をLHからRHに切り替える場合は、--collection=LH を --collection=RH に置き換えて下さい。
また、--out=Vision_LH.csvを--out=Vision_RH.csvに置き換えて下さい。

### 全データをCSVファイルに出力する
```
mongoexport --db=Vision --collection=LH --type=csv \
  --fields=VisionDate,VisionTime,VisionSerialNo,VisionCarModelNo,VisionSukibaNo,VisionCutoffMeasurement,SC01H,SC01L,SC20W,SC21H,SC21L,SF01H,SF01L,SF01W,SF20W,SF21H,SF21L,SF21W,SR01H,SR01L,SR01W_1,SR01W_2,SR02H,SR02L,SR02W,SR03H,SR03L,SR03W,SW01H,SW01L,SW01W,SW20H,SW20L,SW20W,SW21H,SW21L,SW21W,timestamp \
  --sort='{timestamp: 1}' \
  --out=Vision_LH.csv
```

### 期間 2019/01/01 ~ 2020/01/01 のみをデータをCSVに出力する
```
mongoexport --db=Vision --collection=LH --type=csv \
  --fields=VisionDate,VisionTime,VisionSerialNo,VisionCarModelNo,VisionSukibaNo,VisionCutoffMeasurement,SC01H,SC01L,SC20W,SC21H,SC21L,SF01H,SF01L,SF01W,SF20W,SF21H,SF21L,SF21W,SR01H,SR01L,SR01W_1,SR01W_2,SR02H,SR02L,SR02W,SR03H,SR03L,SR03W,SW01H,SW01L,SW01W,SW20H,SW20L,SW20W,SW21H,SW21L,SW21W,timestamp \
  --query='{"timestamp": {"$gte": {"$date": "2019-01-01T00:00:00.000Z"}, "$lt": {"$date": "2020-01-01T00:00:00.000Z"}}}' \
  --sort='{timestamp: 1}' \
  --out=Vision_LH.csv
```

### 車種="44" のみをCSVに出力する
```
mongoexport --db=Vision --collection=LH --type=csv \
  --fields=VisionDate,VisionTime,VisionSerialNo,VisionCarModelNo,VisionSukibaNo,VisionCutoffMeasurement,SC01H,SC01L,SC20W,SC21H,SC21L,SF01H,SF01L,SF01W,SF20W,SF21H,SF21L,SF21W,SR01H,SR01L,SR01W_1,SR01W_2,SR02H,SR02L,SR02W,SR03H,SR03L,SR03W,SW01H,SW01L,SW01W,SW20H,SW20L,SW20W,SW21H,SW21L,SW21W,timestamp \
  --query='{"VisionCarModelNo": {"$eq": "44"}}' \
  --sort='{timestamp: 1}' \
  --out=Vision_LH.csv
```

### スキーバ="1" のみをCSVに出力する
```
mongoexport --db=Vision --collection=LH --type=csv \
  --fields=VisionDate,VisionTime,VisionSerialNo,VisionCarModelNo,VisionSukibaNo,VisionCutoffMeasurement,SC01H,SC01L,SC20W,SC21H,SC21L,SF01H,SF01L,SF01W,SF20W,SF21H,SF21L,SF21W,SR01H,SR01L,SR01W_1,SR01W_2,SR02H,SR02L,SR02W,SR03H,SR03L,SR03W,SW01H,SW01L,SW01W,SW20H,SW20L,SW20W,SW21H,SW21L,SW21W,timestamp \
  --query='{"VisionSukibaNo": {"$eq": "1"}}' \
  --sort='{timestamp: 1}' \
  --out=Vision_LH.csv
```

### 期間 2019/01/01 ~ 2020/01/01, 車種="44", スキーバ="1" を条件として、CSVヘッダーを車種、スキーバ、指定部位(SC01H,SC01L,SC20W),タイムスタンプを指定してCSVに出力する
```
mongoexport --db=Vision --collection=LH --type=csv \
  --fields=VisionCarModelNo,VisionSukibaNo,SC01H,SC01L,SC20W,timestamp \
  --query='{"VisionCarModelNo": {"$eq": "44"}, "VisionSukibaNo": {"$eq": "1"}, "timestamp": {"$gte": {"$date": "2019-01-01T00:00:00.000Z"}, "$lt": {"$date": "2020-01-01T00:00:00.000Z"}}}' \
  --sort='{timestamp: 1}' \
  --out=Vision_LH.csv
```