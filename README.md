# keyence-data-to-csv-at-20200430

### MongoDBのデータをCSVファイルに出力する
ビジョン測定履歴のデータの出力方法は、[Vision.md](./mongo_to_csv/Vision.md) を確認して下さい。

歪測定履歴のデータの出力方法は、[Distortion.md](./mongo_to_csv/Distortion.md) を確認して下さい。

### マイクロサービスの実行方法
[service_broker_go.md](./service_broker_go/service_broker_go.md) を確認して下さい。

### マイクロサービスが出力したh5 (HDF5) をCSVファイルに出力する
[h5_to_csv.md](./h5_to_csv/h5_to_csv.md) を確認して下さい。
