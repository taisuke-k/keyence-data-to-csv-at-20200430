## ServiceBrokerGo の実行方法
Status Json Moduleを起動する
```
cd /home/toyota/anemoi/BackendService/status-json-module/StatusJsonModule
python3 StatusJsonServer.py
```

API Gatewayを起動する
```
cd /home/toyota/anemoi/APIGateway/api-gateway
yarn start
```

起点となる C JSONファイルを配置する。実行するごとに実施して下さい。
```
cd /home/toyota/anemoi/Data/prepare-h5-data-for-targeted-period
cp json/C_0_PrepareH5DataForTargetedPeriod.json .
# リネーム (同じC JSONは続けて実行できないため)
mv C_0_PrepareH5DataForTargetedPeriod.json C_20200430123456_PrepareH5DataForTargetedPeriod.json
```

ServiceBrokerGoを起動する。
```
cd /home/toyota/anemoi/ServiceBrokerGo
./main
```

## MySQLで設定を変更する
MySQL Workbench からテーブルの値を変更して下さい。

### 計算する車種を指定します。
```
"Robot" データベースの "vehicles" テーブルを選択して下さい。
計算したい車種の "calculable" カラムの値を 1 に変更します。
画面右下の "Apply" ボタンをクリックします。
```

### 計算するスキーバを指定します。
```
"Robot" データベースの "sukibas" テーブルを選択して下さい。
計算したいスキーバの "calculable" カラムの値を 1 に変更します。
画面右下の "Apply" ボタンをクリックします。
```

### 計算する部品を指定します。
```
"Robot" データベースの "parts" テーブルを選択して下さい。
計算したい部品の "calculable" カラムの値を 1 に変更します。
画面右下の "Apply" ボタンをクリックします。
```

### 異常検知のマイクロサービスが使用するしきい値を変更する
```
"Robot" データベースの "thresholds" テーブルを選択して下さい。
任意のマイクロサービスの "threshold" カラムの値を任意の値に変更します。
マイクロサービスはmicroservice_idが、"ServiceBroker" データベースの "microservices" テーブルに紐付いています。 
画面右下の "Apply" ボタンをクリックします。
```

### 計算期間、計算間隔を変更する
```
"Robot" データベースの "run" テーブルを選択して下さい。
計算開始期間は "start_timestamp" カラムの値を変更します。
計算終了期間は "end_timestamp" カラムの値を変更します。
計算間隔は "step_timestamp" カラムの値を変更します。
画面右下の "Apply" ボタンをクリックします。
```
