
## h5_to_csv.py について
[h5_to_csv.py](./h5_to_csv.py) は、マイクロサービスが出力した h5 (HDF5)ファイルをCSVファイルに出力します。

### 使い方
python3 h5_to_csv.py {読み込むh5ファイル名} {出力するCSVファイル名}
```
python3 h5_to_csv.py /home/toyota/anemoi/Data/average-value-calculation/file/output/1,1,2019-01-01T00:00:00,2
019-12-30T23:59:59,60.0_2_1.h5 test.csv average-value-calculation.csv
```

### 補足
h5_to_csv.py は下記マイクロサービスの出力するh5ファイルからCSVファイルを出力します。
```
average-value-calculation
detect-abnormal-data-of-average
detect-abnormal-data-of-difference-between-maximun-and-minimun
detect-abnormal-data-of-distance-1d
detect-abnormal-data-of-distance-2d
detect-abnormal-data-of-distance-3d
detect-abnormal-data-of-standard-deviation
difference-between-maximun-and-minimun-calculation
distance-calculation-in-1d-for-all-possible-patterns
distance-calculation-in-2d-for-all-possible-patterns
distance-calculation-in-3d-for-all-possible-patterns
h5-data-sorting-pinponted
standard-deviation-calculation
```

データの形式上の理由で下記のマイクロサービスの出力するh5ファイルは対象外です。
```
prepare-h5-data-for-targeted-period
h5-data-sorting-in-unit-time
```