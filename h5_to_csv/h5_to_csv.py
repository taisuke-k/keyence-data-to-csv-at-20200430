import csv
import os
import sys
from pathlib import Path

import h5py
import numpy as np
import tqdm


MICROSERVICE_LIST = [
    'average-value-calculation',
    'detect-abnormal-data-of-average',
    'detect-abnormal-data-of-difference-between-maximun-and-minimun',
    'detect-abnormal-data-of-distance-1d',
    'detect-abnormal-data-of-distance-2d',
    'detect-abnormal-data-of-distance-3d',
    'detect-abnormal-data-of-standard-deviation',
    'difference-between-maximun-and-minimun-calculation',
    'distance-calculation-in-1d-for-all-possible-patterns',
    'distance-calculation-in-2d-for-all-possible-patterns',
    'distance-calculation-in-3d-for-all-possible-patterns',
    'h5-data-sorting-in-unit-time',
    'h5-data-sorting-pinponted',
    'prepare-h5-data-for-targeted-period',
    'standard-deviation-calculation',
]
MAP = {
    'detect-abnormal-data-of-distance-1d': ['W', 'H', 'L'],
    'detect-abnormal-data-of-distance-2d': ['LH', 'WL', 'HW'],
    'detect-abnormal-data-of-distance-3d': ['WHL'],
    'distance-calculation-in-1d-for-all-possible-patterns': ['W', 'H', 'L'],
    'distance-calculation-in-2d-for-all-possible-patterns': ['LH', 'WL', 'HW'],
    'distance-calculation-in-3d-for-all-possible-patterns': ['WHL'],
}
MAP_KEYS = list(MAP.keys())

REP_KEYS = [
    'detect-abnormal-data-of-average',
    'detect-abnormal-data-of-difference-between-maximun-and-minimun',
    'detect-abnormal-data-of-distance-1d',
    'detect-abnormal-data-of-distance-2d',
    'detect-abnormal-data-of-distance-3d',
    'detect-abnormal-data-of-standard-deviation',
]


def main(h5_file_name, csv_file_name, microservice_name):
    _dict = {}

    print('Start reading a CSV file.')
    with h5py.File(h5_file_name, 'r') as f:
        for group_name in f['data'].keys():
            for dataset_name in f['data'][group_name].keys():
                arr = f['data'][group_name][dataset_name][()]
                if microservice_name in REP_KEYS:
                    arr = np.where(arr, 1, 0)

                if microservice_name not in MAP_KEYS:
                    print(f'{group_name}{dataset_name} => {arr.shape}')
                    _dict[group_name + dataset_name] = arr.tolist()
                else:
                    for i, _arr in enumerate(arr):
                        print(f'{group_name}{dataset_name}{MAP[microservice_name][i]} => {_arr.shape}')
                        _dict[group_name + dataset_name + MAP[microservice_name][i]] = _arr.tolist()

        arr = f['timestamp'][()]
        _dict['timestamp'] = arr.tolist()
        del arr
    print('End reading a CSV file.')

    # Check array size
    array_size = len(_dict['timestamp'])
    for key in _dict.keys():
        assert len(_dict[key]) == array_size

    print('Start writing a CSV file.')
    with open(csv_file_name, 'w') as fw:
        writer = csv.writer(fw)
        # Header
        header = list(_dict.keys())
        writer.writerow(header)
        # Data
        for i in tqdm.tqdm(range(array_size)):
            row = []
            for key in header:
                row.append(_dict[key][i])
            writer.writerow(row)
    print('End writing a CSV file.')
    print(f'Save {csv_file_name}.')
    return


if __name__ == "__main__":
    if len(sys.argv) < 3:
        print('第一引数で入力されるHDF5ファイル名、第二引数で出力するCSVのファイル名を指定してください。')
        print('例:')
        print('python3 h5_to_csv.py /path/to/h5_file_name.h5 /path/to/csv_file_name.csv')
        sys.exit(1)

    h5_file_name = sys.argv[1]
    if not os.path.exists(h5_file_name):
        print('指定されたHDF5ファイルは存在しません。')
        print(h5_file_name)
        sys.exit(1)

    microservice_name = Path(h5_file_name).parent.parent.parent.name
    if microservice_name not in MICROSERVICE_LIST:
        print('Data/file/output ディレクトリに含まれるh5ファイルのパスを絶対パスで指定してください。')
        print('例')
        print('/home/toyota/anemoi/Data/detect-abnormal-data-of-distance-1d/file/output/1,1,2019-01-01T00:00:00,2019-12-30T23:59:59,60.0_1_1_d1.h5')
        sys.exit(1)

    csv_file_name = sys.argv[2]

    main(h5_file_name, csv_file_name, microservice_name)
